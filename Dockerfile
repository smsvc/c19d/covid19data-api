FROM python

COPY ./requirements.txt /
COPY config.py /
COPY ./app /app

ADD https://cdn.jsdelivr.net/npm/swagger-ui-dist@4/swagger-ui-bundle.js static/
ADD https://cdn.jsdelivr.net/npm/swagger-ui-dist@4/swagger-ui.css static/
ADD https://fastapi.tiangolo.com/img/favicon.png static/

RUN pip install -r /requirements.txt

EXPOSE 8000
CMD ["uvicorn", "app.main:api", "--host", "0.0.0.0"]
