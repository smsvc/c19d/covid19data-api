# COVID-19 Data API

This project provides an API to get COVID19 data per district (Landkreis), state (Bundesland) and country.

## Dependencies

`pip install -r requirements.txt`

## Usage

### Local influxdb instance

```
docker run -p 8086:8086 \
      -e INFLUXDB_HTTP_AUTH_ENABLED="true" \
      -e INFLUXDB_ADMIN_USER="admin" \
      -e INFLUXDB_DB="c19d" \
      -e INFLUXDB_USER="influx" \
      -e INFLUXDB_USER_PASSWORD="CorrectHorseBatteryStaple" \
      influxdb:1.8
```

### Import/Collect data

Use [c19d/collector](https://gitlab.com/smsvc/c19d/collector) to initially fill the database.

### API

```
bash static/fetch # once
uvicorn app.main:api
```

or

```
docker build -t covid19data-api .
docker run -p 8000:8000 covid19data-api
```

### Interactive API docs

http://localhost:8000/docs

## Tests

```
pip install tox
tox -a	# list of all defined environments
tox			# run all tests
```

## License

![Creative Commons License](https://i.creativecommons.org/l/by-sa/4.0/88x31.png)

This work is licensed under a [Creative Commons Attribution-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-sa/4.0/).
