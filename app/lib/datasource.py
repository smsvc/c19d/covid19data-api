#!/usr/bin/env python
# -*- encoding: utf-8; py-indent-offset: 4 -*-

# Author: Sebastian Mark
# CC-BY-SA (https://creativecommons.org/licenses/by-sa/4.0/deed.de)
# for civil use only

import hashlib
import re
import time

import config as cfg

from influxdb import InfluxDBClient

import pandas as pd

db = InfluxDBClient(**cfg.influx_config)

cache = {}
cache_timeout = cfg.cache_timeout


def cached_db_query(qry: str) -> dict:
    """
    cache db queries for $cache_timeout

    Arguments:
        query string

    Returns:
       query results (dict)
    """
    key = hashlib.sha512(qry.encode()).hexdigest()
    try:
        cache[f"{key}.data"]
        cache_age = int(time.time() - cache[f"{key}.ts"])
        assert cache_age < cache_timeout
    except (KeyError, AssertionError):
        cache[f"{key}.data"] = db.query(qry)
        cache[f"{key}.ts"] = time.time()
    return cache[f"{key}.data"]


def get_de_history():
    """
    get all RKI datasets for country

    Arguments:
        none

    Returns:
        country history (DataFrame)
    """
    data = cached_db_query(
        "SELECT sum(*) FROM de"
        + " WHERE type='deaths' OR type='cases' OR type='icu'"
        + " GROUP BY time(1d) fill(none)"
    )
    de_history = pd.DataFrame.from_dict(data.get_points())
    de_history.rename(columns=(lambda x: re.sub("^sum_", "", x)), inplace=True)
    de_history["Date"] = de_history["time"].str.replace("T00:00:00Z", "")
    de_history = de_history.fillna("n/a")
    return de_history


def get_state_list():
    """
    get list of all states

    Arguments:
        none

    Returns:
       states (DataFrame)
    """
    data = cached_db_query("SELECT * FROM states WHERE type='references'")
    states = pd.DataFrame.from_dict(data.get_points())
    return states.sort_values(by="State")


def get_state_history(id: str):
    """
    get all RKI datasets for passed state id

    Arguments:
        state ID (string)

    Returns:
        state history (DataFrame)
    """
    data = cached_db_query(
        f"SELECT sum(*) FROM states WHERE state = '{id}'"
        + " AND (type='deaths' OR type='cases' OR type='icu')"
        + " GROUP BY time(1d) fill(none)"
    )
    state_history = pd.DataFrame.from_dict(data.get_points())
    if state_history.empty:
        return pd.DataFrame()
    state_history.rename(columns=(lambda x: re.sub("^sum_", "", x)), inplace=True)
    state_history["Date"] = state_history["time"].str.replace("T00:00:00Z", "")
    state_history = state_history.fillna("n/a")
    return state_history


def get_district_list():
    """
    get list of all districts

    Arguments:
        none

    Returns:
       districts (DataFrame)
    """
    data = cached_db_query("SELECT * FROM districts WHERE type='references'")
    districts = pd.DataFrame.from_dict(data.get_points())
    districts["ID"] = districts["district"]
    return districts.sort_values(by="ID")


def get_district_history(id: str):
    """
    get RKI datasets for passed district id

    Arguments:
        district ID (string)

    Returns:
        district history (DataFrame)
    """
    data = cached_db_query(
        f"SELECT sum(*) FROM districts WHERE district = '{id}'"
        + " AND (type='deaths' OR type='cases' OR type='icu')"
        + " GROUP BY time(1d) fill(none)"
    )
    district_history = pd.DataFrame.from_dict(data.get_points())
    if district_history.empty:
        return pd.DataFrame()
    district_history.rename(columns=(lambda x: re.sub("^sum_", "", x)), inplace=True)
    district_history["Date"] = district_history["time"].str.replace("T00:00:00Z", "")
    district_history = district_history.fillna("n/a")
    return district_history
