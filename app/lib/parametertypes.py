#!/usr/bin/env python
# -*- encoding: utf-8; py-indent-offset: 4 -*-

# Author: Sebastian Mark
# CC-BY-SA (https://creativecommons.org/licenses/by-sa/4.0/deed.de)
# for civil use only

ascii_regex = r"^[- \wÄäÖöÜüß]+$"
alpha_regex = r"^[A-ZÖÄÜ]+$"
num_regex = r"^\d+$"
