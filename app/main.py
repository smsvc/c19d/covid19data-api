#!/usr/bin/env python
# -*- encoding: utf-8; py-indent-offset: 4 -*-

# Author: Sebastian Mark
# CC-BY-SA (https://creativecommons.org/licenses/by-sa/4.0/deed.de)
# for civil use only

from app.routers import de, districts, states

from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from fastapi.openapi.docs import get_swagger_ui_html
from fastapi.staticfiles import StaticFiles


api = FastAPI(
    title="covid19 data api",
    version="2.2.2",
    root_path="/api/v2",
    contact={
        "name": "Home",
        "url": "https://c19d.smsvc.net",
    },
    docs_url=None,
    redoc_url=None,
)

api.mount("/static", StaticFiles(directory="static"), name="static")


@api.get("/docs", include_in_schema=False)
async def custom_swagger_ui_html():
    return get_swagger_ui_html(
        openapi_url=api.root_path + api.openapi_url,
        title=api.title + " - Swagger UI",
        swagger_js_url=api.root_path + "/static/swagger-ui-bundle.js",
        swagger_css_url=api.root_path + "/static/swagger-ui.css",
        swagger_favicon_url=api.root_path + "/static/favicon.png",
    )


api.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

api.include_router(districts.router)
api.include_router(states.router)
api.include_router(de.router)
