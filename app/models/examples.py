#!/usr/bin/env python
# -*- encoding: utf-8; py-indent-offset: 4 -*-

# Author: Christian Hennevogl
# CC-BY-SA (https://creativecommons.org/licenses/by-sa/4.0/deed.de)
# for civil use only

startdate = {
    "good": {"summary": "Working example", "value": "2021-01-01"},
    "bad": {"summary": "Incorrect Example", "value": "abc"},
}
enddate = {
    "good": {"summary": "Working example", "value": "2021-01-14"},
    "bad": {"summary": "Incorrect Example", "value": "abc"},
}
lk_name = {
    "good": {"summary": "Working example", "value": "Bonn"},
    "bad": {"summary": "Incorrect Example", "value": "08/15"},
}
bl_name = {
    "good": {"summary": "Working example", "value": "Nordrhein-Westfalen"},
    "bad": {"summary": "Incorrect Example", "value": "08/15"},
}
lk_type = {
    "good": {"summary": "Working example", "value": "Kreisfreie Stadt"},
    "bad": {"summary": "Incorrect Example", "value": "08/15"},
}
population_min = {
    "good": {"summary": "Working example", "value": 300000},
    "bad": {"summary": "Incorrect Example", "value": "abc"},
}
population_max = {
    "good": {"summary": "Working example", "value": 18e6},
    "bad": {"summary": "Incorrect Example", "value": "abc"},
}
district_id = {
    "good": {"summary": "Working example", "value": "05314"},
    "bad": {"summary": "Incorrect Example", "value": "abc"},
}
state_code = {
    "good": {"summary": "Working example", "value": "NW"},
    "bad": {"summary": "Incorrect Example", "value": "abc"},
}
