#!/usr/bin/env python
# -*- encoding: utf-8; py-indent-offset: 4 -*-

# Author: Christian Hennevogl
# CC-BY-SA (https://creativecommons.org/licenses/by-sa/4.0/deed.de)
# for civil use only

from typing import List, Union

from pydantic import BaseModel, Field


class Message(BaseModel):
    message: str


class RKIDataSet(BaseModel):
    Date: str
    Cases: int
    Cases_Last_Week: int
    Cases_Last_Week_Per_100000: float
    Deaths: int
    Deaths_Last_Week: int
    Deaths_Last_Week_Per_100000: float


class RKIDataSets(BaseModel):
    __root__: List[RKIDataSet]


class RKIDIVIDataSet(RKIDataSet):
    ICU_Cases: Union[int, str]
    ICU_Cases_Last_Week: Union[int, str]
    ICU_Cases_Last_Week_Per_100000: Union[float, str]


class RKIDIVIDataSets(BaseModel):
    __root__: List[RKIDIVIDataSet]


class District(BaseModel):
    ID: str
    Population: int
    BL_Name: str
    BL_Code: str
    LK_Name: str
    LK_Typ: str


class Districts(BaseModel):
    __root__: List[District]


class State(BaseModel):
    State: str
    Code: str
    Population: int
    Pop_Density: int = Field(alias="Pop Density")


class States(BaseModel):
    __root__: List[State]
