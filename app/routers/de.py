#!/usr/bin/env python
# -*- encoding: utf-8; py-indent-offset: 4 -*-

# Author: Sebastian Mark
# CC-BY-SA (https://creativecommons.org/licenses/by-sa/4.0/deed.de)
# for civil use only

from datetime import date

import app.lib.datasource as source
import app.models.examples as example
from app.models.models import RKIDIVIDataSet, RKIDIVIDataSets

from fastapi import APIRouter, Query

import pandas as pd

router = APIRouter()


@router.get(
    "/de/data",
    tags=["country"],
    response_model=RKIDIVIDataSets,
    summary="Get data history for country",
)
async def get_de_data_with_filter(
    start: date = Query(
        date(1970, 1, 1),
        description="start date",
        examples=example.startdate,
    ),
    end: date = Query(
        date(2038, 1, 19),
        description="end date",
        examples=example.enddate,
    ),
):
    de_history = source.get_de_history()

    # new tmp datetime entry
    de_history["dt"] = pd.to_datetime(de_history["Date"])

    # filter Dataframe by date
    de_history = de_history[de_history["dt"].dt.date >= start]
    de_history = de_history[de_history["dt"].dt.date <= end]

    # drop entry as "not JSON serializable"
    de_history = de_history.drop(columns="dt")

    return de_history.to_dict(orient="records")


@router.get(
    "/de/data/last",
    tags=["country"],
    response_model=RKIDIVIDataSet,
    summary="Get last data entry for country",
)
async def get_de_data():
    de_history = source.get_de_history()
    de = de_history.iloc[-1]
    return de.to_dict()
