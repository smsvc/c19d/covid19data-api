#!/usr/bin/env python
# -*- encoding: utf-8; py-indent-offset: 4 -*-

# Author: Sebastian Mark
# CC-BY-SA (https://creativecommons.org/licenses/by-sa/4.0/deed.de)
# for civil use only

from datetime import date

import app.lib.datasource as source
import app.models.examples as example
from app.lib.parametertypes import ascii_regex, num_regex
from app.models.models import District, Districts, Message, RKIDataSet, RKIDataSets

from fastapi import APIRouter, Path, Query
from fastapi.responses import JSONResponse

import pandas as pd

router = APIRouter()


@router.get(
    "/districts",
    tags=["district"],
    response_model=Districts,
    summary="List and search districts",
)
async def search_district_by_name(
    LK_Name: str = Query(
        None, description="district name", regex=ascii_regex, examples=example.lk_name
    ),
    BL_Name: str = Query(
        None, description="state name", regex=ascii_regex, examples=example.bl_name
    ),
    LK_Typ: str = Query(
        None, description="district type", regex=ascii_regex, examples=example.lk_type
    ),
    population_min: int = Query(
        0, description="minimum population", ge=0, examples=example.population_min
    ),
    population_max: int = Query(
        80e6, description="maximum population", ge=0, examples=example.population_max
    ),
):
    districts = source.get_district_list()

    found = districts[
        districts["LK_Name"].str.contains(LK_Name or "", case=False)
        & districts["BL_Name"].str.contains(BL_Name or "", case=False)
        & districts["LK_Typ"].str.contains(LK_Typ or "", case=False)
        & (districts["Population"] >= population_min)
        & (districts["Population"] <= population_max)
    ]

    return found.to_dict(orient="records")


@router.get(
    "/districts/{district_id}",
    tags=["district"],
    response_model=District,
    responses={404: {"model": Message}},
    summary="Get a district",
)
async def get_district_by_id(
    district_id: str = Path(
        None, min_length=5, max_length=5, regex=num_regex, examples=example.district_id
    ),
):
    districts = source.get_district_list()
    district = districts[districts["ID"] == district_id]
    if district.empty:
        return JSONResponse(status_code=404, content={"message": "district not found"})
    return district.to_dict(orient="records")[0]


@router.get(
    "/districts/{district_id}/data",
    tags=["district"],
    response_model=RKIDataSets,
    responses={404: {"model": Message}},
    summary="Get data history for district",
)
async def get_data_by_id_with_filter(
    district_id: str = Path(
        None, min_length=5, max_length=5, regex=num_regex, examples=example.district_id
    ),
    start: date = Query(
        date(1970, 1, 1),
        description="start date",
        examples=example.startdate,
    ),
    end: date = Query(
        date(2038, 1, 19),
        description="end date",
        examples=example.enddate,
    ),
):
    district_history = source.get_district_history(district_id)
    if district_history.empty:
        return JSONResponse(status_code=404, content={"message": "district not found"})

    # new tmp datetime entry
    district_history["dt"] = pd.to_datetime(district_history["Date"])

    # filter Dataframe by date
    district_history = district_history[district_history["dt"].dt.date >= start]
    district_history = district_history[district_history["dt"].dt.date <= end]

    # drop entry as "not JSON serializable"
    district_history = district_history.drop(columns="dt")

    return district_history.to_dict(orient="records")


@router.get(
    "/districts/{district_id}/data/last",
    tags=["district"],
    response_model=RKIDataSet,
    responses={404: {"model": Message}},
    summary="Get last data entry for district",
)
async def get_data_by_id(
    district_id: str = Path(
        None, min_length=5, max_length=5, regex=num_regex, examples=example.district_id
    ),
):
    district_history = source.get_district_history(district_id)
    if district_history.empty:
        return JSONResponse(status_code=404, content={"message": "district not found"})
    district = district_history.iloc[-1]
    return district.to_dict()
