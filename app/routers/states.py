#!/usr/bin/env python
# -*- encoding: utf-8; py-indent-offset: 4 -*-

# Author: Sebastian Mark
# CC-BY-SA (https://creativecommons.org/licenses/by-sa/4.0/deed.de)
# for civil use only

from datetime import date

import app.lib.datasource as source
import app.models.examples as example
from app.lib.parametertypes import alpha_regex, ascii_regex
from app.models.models import Message, RKIDIVIDataSet, RKIDIVIDataSets, State, States

from fastapi import APIRouter, Path, Query
from fastapi.responses import JSONResponse

import pandas as pd

router = APIRouter()


@router.get(
    "/states",
    tags=["state"],
    response_model=States,
    summary="List and search states",
)
async def search_state_by_name(
    name: str = Query(
        None, description="state name", regex=ascii_regex, examples=example.bl_name
    ),
    population_min: int = Query(
        0, description="minimum population", ge=0, examples=example.population_min
    ),
    population_max: int = Query(
        80e6, description="maximum population", ge=0, examples=example.population_max
    ),
):
    states = source.get_state_list()
    found = states[
        states["State"].str.contains(name or "", case=False)
        & (states["Population"] >= population_min)
        & (states["Population"] <= population_max)
    ]
    return found.to_dict(orient="records")


@router.get(
    "/states/{state_code}",
    tags=["state"],
    response_model=State,
    responses={404: {"model": Message}},
    summary="Get a state",
)
async def get_state_by_code(
    state_code: str = Path(
        None, min_length=2, max_length=2, regex=alpha_regex, examples=example.state_code
    ),
):
    states = source.get_state_list()
    state = states[states["Code"] == state_code]
    if state.empty:
        return JSONResponse(status_code=404, content={"message": "state not found"})
    return state.to_dict(orient="records")[0]


@router.get(
    "/states/{state_code}/data",
    tags=["state"],
    response_model=RKIDIVIDataSets,
    responses={404: {"model": Message}},
    summary="Get data history for state",
)
async def get_data_by_code_with_filter(
    state_code: str = Path(
        None, min_length=2, max_length=2, regex=alpha_regex, examples=example.state_code
    ),
    start: date = Query(
        date(1970, 1, 1),
        description="start date",
        examples=example.startdate,
    ),
    end: date = Query(
        date(2038, 1, 19),
        description="end date",
        examples=example.enddate,
    ),
):
    state_history = source.get_state_history(state_code)
    if state_history.empty:
        return JSONResponse(status_code=404, content={"message": "state not found"})

    # new tmp datetime entry
    state_history["dt"] = pd.to_datetime(state_history["Date"])

    # filter Dataframe by date
    state_history = state_history[state_history["dt"].dt.date >= start]
    state_history = state_history[state_history["dt"].dt.date <= end]

    # drop entry as "not JSON serializable"
    state_history = state_history.drop(columns="dt")

    return state_history.to_dict(orient="records")


@router.get(
    "/states/{state_code}/data/last",
    tags=["state"],
    response_model=RKIDIVIDataSet,
    responses={404: {"model": Message}},
    summary="Get last data entry for state",
)
async def get_data_by_code(
    state_code: str = Path(
        None, min_length=2, max_length=2, regex=alpha_regex, examples=example.state_code
    ),
):
    state_history = source.get_state_history(state_code)
    if state_history.empty:
        return JSONResponse(status_code=404, content={"message": "state not found"})
    state = state_history.iloc[-1]
    return state.to_dict()
