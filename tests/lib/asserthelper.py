#!/usr/bin/env python
# -*- encoding: utf-8; py-indent-offset: 4 -*-

# Author: Sebastian Mark
# CC-BY-SA (https://creativecommons.org/licenses/by-sa/4.0/deed.de)
# for civil use only

import requests


def response_fail_with_code(response: requests.models.Response, code: int):
    assert response.status_code == code


def response_is_ok(response: requests.models.Response):
    assert response.status_code == 200
    assert response.headers["content-type"] == "application/json"


def response_is_list(response: requests.models.Response):
    response_is_ok(response)
    assert response.json().__class__ == list


def response_is_dict(response: requests.models.Response):
    response_is_ok(response)
    assert response.json().__class__ == dict


def response_is_list_with_length(response: requests.models.Response, length: int):
    response_is_list(response)
    assert len(response.json()) == length


def response_is_dict_with_length(response: requests.models.Response, length: int):
    response_is_dict(response)
    assert len(response.json()) == length
