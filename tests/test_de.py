#!/usr/bin/env python
# -*- encoding: utf-8; py-indent-offset: 4 -*-

# Author: Sebastian Mark
# CC-BY-SA (https://creativecommons.org/licenses/by-sa/4.0/deed.de)
# for civil use only

from app.main import api

from fastapi.testclient import TestClient

import lib.asserthelper as check

client = TestClient(api)


def test_country():
    response = client.get("/de/data")
    check.response_is_list(response)
    assert len(response.json()) > 500

    response = client.get("/de/data?start=2021-07-01&end=2021-07-07")
    check.response_is_list_with_length(response, 7)

    response = client.get("/de/data?start=2036-01-01&end=2036-01-01")
    check.response_is_list_with_length(response, 0)

    response = client.get("/de/data?start=foobar")
    check.response_fail_with_code(response, 422)

    response = client.get("/de/data/last")
    check.response_is_dict_with_length(response, 10)
