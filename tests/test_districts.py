#!/usr/bin/env python
# -*- encoding: utf-8; py-indent-offset: 4 -*-

# Author: Sebastian Mark
# CC-BY-SA (https://creativecommons.org/licenses/by-sa/4.0/deed.de)
# for civil use only

from app.main import api

from fastapi.testclient import TestClient

import lib.asserthelper as check

client = TestClient(api)


def test_districts():
    response = client.get("/districts")
    check.response_is_list_with_length(response, 411)

    response = client.get("/districts?LK_Name=Bonn")
    check.response_is_list_with_length(response, 1)

    response = client.get("/districts?LK_Name=Köln")
    check.response_is_list_with_length(response, 1)

    response = client.get("/districts?LK_Name=Foobar")
    check.response_is_list_with_length(response, 0)

    response = client.get("/districts?LK_Name=?")
    check.response_fail_with_code(response, 422)

    response = client.get("/districts?BL_Name=Schleswig-Holstein")
    check.response_is_list_with_length(response, 15)

    response = client.get("/districts?BL_Name=Foobar")
    check.response_is_list_with_length(response, 0)

    response = client.get("/districts?BL_Name=?")
    check.response_fail_with_code(response, 422)

    response = client.get("/districts?LK_Typ=Kreis")
    check.response_is_list_with_length(response, 399)

    response = client.get("/districts?LK_Typ=Foobar")
    check.response_is_list_with_length(response, 0)

    response = client.get("/districts?LK_Typ=?")
    check.response_fail_with_code(response, 422)

    response = client.get("/districts?LK_Name=Bonn&LK_Typ=KeinKreis")
    check.response_is_list_with_length(response, 0)

    response = client.get("/districts?LK_Name=&BL_Name=&LK_Typ=")
    check.response_fail_with_code(response, 422)

    response = client.get("/districts?population_min=1847253")
    check.response_is_list_with_length(response, 1)

    response = client.get("/districts?population_max=34193")
    check.response_is_list_with_length(response, 1)

    response = client.get("/districts?population_min=80000000")
    check.response_is_list_with_length(response, 0)

    response = client.get("/districts?population_min=0&population_max=0")
    check.response_is_list_with_length(response, 0)

    response = client.get("/districts?population_min=abc")
    check.response_fail_with_code(response, 422)

    response = client.get("/districts?population_max=abc")
    check.response_fail_with_code(response, 422)

    response = client.get("/districts?population_min=-12")
    check.response_fail_with_code(response, 422)

    response = client.get("/districts?population_max=-12")
    check.response_fail_with_code(response, 422)

    response = client.get("/districts/05314")
    check.response_is_dict_with_length(response, 6)

    response = client.get("/districts/00000")
    check.response_fail_with_code(response, 404)

    response = client.get("/districts/1234")
    check.response_fail_with_code(response, 422)

    response = client.get("/districts/123456")
    check.response_fail_with_code(response, 422)

    response = client.get("/districts/abcde")
    check.response_fail_with_code(response, 422)

    response = client.get("/districts/05314/data")
    check.response_is_list(response)
    assert len(response.json()) > 500

    response = client.get("/districts/00000/data")
    check.response_fail_with_code(response, 404)

    response = client.get("/districts/1234/data")
    check.response_fail_with_code(response, 422)

    response = client.get("/districts/123456/data")
    check.response_fail_with_code(response, 422)

    response = client.get("/districts/abcde/data")
    check.response_fail_with_code(response, 422)

    response = client.get("/districts/05314/data?start=2021-07-01&end=2021-07-07")
    check.response_is_list_with_length(response, 7)

    response = client.get("/districts/05314/data?start=2036-01-01&end=2036-01-01")
    check.response_is_list_with_length(response, 0)

    response = client.get("/districts/05314/data?start=foobar")
    check.response_fail_with_code(response, 422)

    response = client.get("/districts/05314/data/last")
    check.response_is_dict_with_length(response, 7)

    response = client.get("/districts/00000/data/last")
    check.response_fail_with_code(response, 404)

    response = client.get("/districts/1234/data/last")
    check.response_fail_with_code(response, 422)

    response = client.get("/districts/123456/data/last")
    check.response_fail_with_code(response, 422)

    response = client.get("/districts/abcde/data/last")
    check.response_fail_with_code(response, 422)
