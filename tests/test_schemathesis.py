#!/usr/bin/env python
# -*- encoding: utf-8; py-indent-offset: 4 -*-

# Author: Christian Hennevogl
# CC-BY-SA (https://creativecommons.org/licenses/by-sa/4.0/deed.de)
# for civil use only

from app.main import api

from hypothesis import HealthCheck, settings

from schemathesis import from_asgi
from schemathesis.checks import (
    content_type_conformance,
    not_a_server_error,
    response_headers_conformance,
    response_schema_conformance,
    status_code_conformance,
)


schema = from_asgi("/openapi.json", api)


@schema.parametrize()
@settings(
    # max_examples=10,
    suppress_health_check=[HealthCheck.filter_too_much, HealthCheck.too_slow],
)
def test_api(case):
    response = case.call_asgi()
    case.validate_response(
        response,
        checks=(
            response_schema_conformance,
            not_a_server_error,
            response_headers_conformance,
            content_type_conformance,
            status_code_conformance,
        ),
    )
