#!/usr/bin/env python
# -*- encoding: utf-8; py-indent-offset: 4 -*-

# Author: Sebastian Mark
# CC-BY-SA (https://creativecommons.org/licenses/by-sa/4.0/deed.de)
# for civil use only

from app.main import api

from fastapi.testclient import TestClient

import lib.asserthelper as check

client = TestClient(api)


def test_states():
    response = client.get("/states")
    check.response_is_list_with_length(response, 16)

    response = client.get("/states?name=Nordrhein")
    check.response_is_list_with_length(response, 1)

    response = client.get("/states?name=Foobar")
    check.response_is_list_with_length(response, 0)

    response = client.get("/states?name=12344")
    check.response_is_list_with_length(response, 0)

    response = client.get("/states?name=None")
    check.response_is_list_with_length(response, 0)

    response = client.get("/states?name=?")
    check.response_fail_with_code(response, 422)

    response = client.get("/states?name=")
    check.response_fail_with_code(response, 422)

    response = client.get("/states?population_min=17925570")
    check.response_is_list_with_length(response, 1)

    response = client.get("/states?population_max=683000")
    check.response_is_list_with_length(response, 1)

    response = client.get("/states?population_max=0&population_min=0")
    check.response_is_list_with_length(response, 0)

    response = client.get("/states?population_min=80000000")
    check.response_is_list_with_length(response, 0)

    response = client.get("/states?population_min=abc")
    check.response_fail_with_code(response, 422)

    response = client.get("/states?population_max=abc")
    check.response_fail_with_code(response, 422)

    response = client.get("/states?population_min=-12")
    check.response_fail_with_code(response, 422)

    response = client.get("/states?population_max=-12")
    check.response_fail_with_code(response, 422)

    response = client.get("/states/NW")
    check.response_is_dict_with_length(response, 4)

    response = client.get("/states/AB")
    check.response_fail_with_code(response, 404)

    response = client.get("/states/A")
    check.response_fail_with_code(response, 422)

    response = client.get("/states/ABC")
    check.response_fail_with_code(response, 422)

    response = client.get("/states/00")
    check.response_fail_with_code(response, 422)

    response = client.get("/states/NW/data")
    check.response_is_list(response)
    assert len(response.json()) > 500

    response = client.get("/states/AB/data")
    check.response_fail_with_code(response, 404)

    response = client.get("/states/A/data")
    check.response_fail_with_code(response, 422)

    response = client.get("/states/ABC/data")
    check.response_fail_with_code(response, 422)

    response = client.get("/states/00/data")
    check.response_fail_with_code(response, 422)

    response = client.get("/states/NW/data?start=2021-07-01&end=2021-07-07")
    check.response_is_list_with_length(response, 7)

    response = client.get("/states/NW/data?start=2036-01-01&end=2036-01-01")
    check.response_is_list_with_length(response, 0)

    response = client.get("/states/NW/data?start=foobar")
    check.response_fail_with_code(response, 422)

    response = client.get("/states/NW/data/last")
    check.response_is_dict_with_length(response, 10)

    response = client.get("/states/AB/data/last")
    check.response_fail_with_code(response, 404)

    response = client.get("/states/A/data/last")
    check.response_fail_with_code(response, 422)

    response = client.get("/states/ABC/data/last")
    check.response_fail_with_code(response, 422)

    response = client.get("/states/00/data/last")
    check.response_fail_with_code(response, 422)
